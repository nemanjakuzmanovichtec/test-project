## What is this PR about?

What is a reason for this change/addition?

## Issue ticket number and link

Feature 123 [Link to the feature](https://www.example.com)

## Background and change summary

What was the approach and implementation details if any

## Checklist before requesting a review

- [ ] I have performed a self-review of my code
- [ ] Code is formatted using the default formatter
- [ ] Passes linting checks
- [ ] Main use cases are tested & the tests are passing
- [ ] Do the changes follow a defined architecture
- Add whatever additional steps are needed for the specific team

## Credits/Paired

with @person1
